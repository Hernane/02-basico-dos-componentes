import React, { Component,Fragment } from 'react'
import Ninja400 from './components/Ninja400'
import product from './productJSON';
import App from './App.css'
import NinjaZX6R from './components/NinjaZX6R';
import Z900 from './components/Z900';
import NinjaH2R from './components/NinjaH2R';
const modelo=<NinjaH2R product={product} />
class ProductPage extends Component {
    render () {
        if(this.props.NinjaH2R.toggle400===true){
            this.modelo=<Ninja400 product={product} />
        }
        console.log(this.props)
        return (
            <Fragment >
                <div className="ProductPage">
                    {this.props.modelo}
                </div>
            </Fragment>
        )
    }
}

export default ProductPage