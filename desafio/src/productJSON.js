export default {
    "productId": 1167,
    "name": " Kawasaki Ninja   ",
    "salesChannel": "1",
    "available": true,
    "dimensionsMap": {
        "tamanho": [
            "400cc",
            "636cc",
            "948cc",
            "998cc"
        ]
    },
    "skus": [
        {
            "sku": 4929,
            "skuname": "Kawasaki Ninja 400",
            "dimensions": {
                "cilindrada": "399cc"
            },
            "available": true,
            "availablequantity": 5,
            "cacheVersionUsedToCallCheckout": null,
            "listPriceFormated": "R$ 26.000,00",
            "listPrice": 26000.00,
            "taxFormated": "R$ 0,00",
            "taxAsInt": 0,
            "bestPriceFormated": "R$ 24.000,00",
            "bestPrice": 24000.00,
            "installments": 0,
            "installmentsValue": 0,
            "installmentsInsterestRate": null,
            "image": "https://content.kawasaki.com/Content/Uploads/Products/7780/Colors/pxm53c0v.n2x.png",
            "sellerId": "1",
            "seller": "Kawasaki",
            "measures": {
                "cubicweight": 1.0000,
                "height": 20.0000,
                "length": 10.0000,
                "weight": 800.0000,
                "width": 15.0000
            },
            "unitMultiplier": 1.0000,
            "rewardValue": 0
        },
        {
            "sku": 4930,
            "skuname": "Kawasaki Ninja ZX6R",
            "dimensions": {
                "cilindrada": "636cc"
            },
            "available": false,
            "availablequantity": 0,
            "cacheVersionUsedToCallCheckout": null,
            "listPriceFormated": "R$ 49.990,00",
            "listPrice": 49990.00,
            "taxFormated": "R$ 0,00",
            "taxAsInt": 0,
            "bestPriceFormated": "R$ 0,00",
            "bestPrice": 0,
            "installments": 0,
            "installmentsValue": 0,
            "installmentsInsterestRate": null,
            "image": "https://content.kawasaki.com/Content/Uploads/Products/7729/Colors/sh4hrrx0.e5y.png",
            "sellerId": "1",
            "seller": "Kawasaki",
            "measures": {
                "cubicweight": 1.0000,
                "height": 20.0000,
                "length": 10.0000,
                "weight": 800.0000,
                "width": 15.0000
            },
            "unitMultiplier": 1.0000,
            "rewardValue": 0
        },
        {
            "sku": 4931,
            "skuname": "Z900",
            "dimensions": {
                "cilindrada": "948cc"
            },
            "available": true,
            "availablequantity": 5,
            "cacheVersionUsedToCallCheckout": "d227bc129516772a2d658db2217cfaf8_",
            "listPriceFormated": "R$ 42.490,00",
            "listPrice": 42490.00,
            "taxFormated": "R$ 0,00",
            "taxAsInt": 0,
            "bestPriceFormated": "R$ 39.000,00",
            "bestPrice": 39990,
            "installments": 12,
            "installmentsValue": 1999,
            "installmentsInsterestRate": 0,
            "image": "https://content.kawasaki.com/Content/uploads/Products/7929/Nav/19ZR900B_40SBK1DRF1CG_A.png?w=792",
            "sellerId": "1",
            "seller": "Kawasaki",
            "measures": {
                "cubicweight": 1.0000,
                "height": 20.0000,
                "length": 10.0000,
                "weight": 800.0000,
                "width": 15.0000
            },
            "unitMultiplier": 1.0000,
            "rewardValue": 0
        },
        {
            "sku": 4932,
            "skuname": "Ninja H2R",
            "dimensions": {
                "cilindrada": "998cc"
            },
            "available": true,
            "availablequantity": 5,
            "cacheVersionUsedToCallCheckout": "d227bc129516772a2d658db2217cfaf8_",
            "listPriceFormated": "R$ 357.000,00",
            "listPrice": 357000.00,
            "taxFormated": "R$ 0,00",
            "taxAsInt": 0,
            "bestPriceFormated": "R$ 357.000,00",
            "bestPrice": 357000.00,
            "installments": 12,
            "installmentsValue": 1999,
            "installmentsInsterestRate": 0,
            "image": "https://content.kawasaki.com/Content/Uploads/Products/7898/Colors/i4yispun.pnh.png",
            "sellerId": "1",
            "seller": "Kawasaki",
            "measures": {
                "cubicweight": 1.0000,
                "height": 20.0000,
                "length": 10.0000,
                "weight": 800.0000,
                "width": 15.0000
            },
            "unitMultiplier": 1.0000,
            "rewardValue": 0
        }
    ]
}