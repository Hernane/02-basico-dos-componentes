import React, { Component,Fragment } from 'react';
import product from './productJSON';
import './App.css';
import ProductPage from './ProductPage';

class App extends Component {
 
 
  render() {
    return (
      <Fragment>
        <div className="App">
          <ProductPage product={product} />
        </div>
      </Fragment>
    );
  }
}

export default App;
