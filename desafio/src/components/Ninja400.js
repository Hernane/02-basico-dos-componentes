import React, { Component,Fragment } from 'react'
import App from '../App.css'
let disponibilidade
let preco
class Ninja400 extends Component {
    constructor(props){
        super(props)
        this.carrinho=this.carrinho.bind(this)
    } 
    carrinho(event){ /*botão para adicionar ao carrinho */
        event.preventDefault()
        alert("Você adicionou ao carrinho o produto: " + 
        this.props.product.skus[0].skuname + " por " + 
        this.props.product.skus[0].bestPriceFormated )
    }
    render () {
        if(this.props.product.skus[0].available===true){ /*verifica se o produto está em estoque e diz quantos estão disponíveis */
            disponibilidade=this.props.product.skus[0].availablequantity+" Em estoque";   
        }
        else{
            disponibilidade="Esgotado"
        }
        
        if(this.props.product.skus[0].listPriceFormated!=this.props.product.skus[0].bestPriceFormated){ /*verifica se o produto tem desconto, se não tiver,apenas 1 valor será apresentado */
            preco=<Fragment>
                    <div className="ninja400de"><del>de {this.props.product.skus[0].listPriceFormated}</del></div>   
                    <div className="ninja400por">por {this.props.product.skus[0].bestPriceFormated}</div>
                  </Fragment>
            }
        else{
            preco=<div className="ninja400por">por {this.props.product.skus[0].bestPriceFormated}</div>
        }
    return ( 
            <Fragment>
                <body className="ninja400background">
                    <div>
                       <div className="banner">
                         <table> {/*lista de produtos */}
                             <td className="drop">
                                <div className="dropdown">
                                    <button className="dropbtn">Motos ˅</button>
                                    <div className="dropdown-content">
                                        <a href="#">Ninja 400</a>
                                        <a href="#">Ninja ZX6R</a>
                                        <a href="#">Z900</a>
                                        <a href="#">Ninja H2R</a>
                                    </div>
                                </div>    
                            </td>
                        </table> 
                            <img className="banner1" 
                            src="http://atvillustrated.com/files/Kawi_3lines__logo_web.jpg" alt="img.png"></img>
                        </div>
                    </div>
                    <div>
                        <img src={this.props.product.skus[0].image} alt="img.png" width="500px"></img>
                        <div className="ninja400">{this.props.product.skus[0].skuname}(  {disponibilidade})</div>
                        <div className="ninja400">{this.props.product.skus[0].dimensions.cilindrada}</div>
                        {preco}
                    </div>
                    <button className="botão400" onClick={this.carrinho}>Adicionar ao carrinho</button>
                </body>
            </Fragment>
        )
    }
}

export default Ninja400