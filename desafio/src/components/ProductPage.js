import React, { Component,Fragment } from 'react'
import App from '../App.css'
let disponibilidade
class ProductPage extends Component {
    constructor(props){
        super(props)
        this.carrinho=this.carrinho.bind(this)
    } 
    carrinho(event){
        event.preventDefault()
        alert("Você adicionou ao carrinho o produto: " + this.props.product.skus[0].skuname + " por " + this.props.product.skus[0].bestPriceFormated )
    }
    render () {
        console.log(this.props)
        /*if(this.props.product.skus[0].availablequantity!=0)
        {
            this.props.product.skus[0].available==true
        }
        else{
            if(this.props.product.skus[0].availablequantity===0)
            {
                this.props.product.skus[0].available==false;
            }
        } */
        if(this.props.product.skus[0].available===true){
            disponibilidade="Em estoque";   
        }
        else{
            disponibilidade="Esgotado"
        }
        return (
            <Fragment>
                <body className="ninja400background">
                  <div>
                        <div className="banner">
                            <table>
                                <td className="drop400">
                                    <div className="dropdown">
                                        <button class="dropbtn">Motos ˅</button>
                                        <div class="dropdown-content">
                                            <a href="#">Ninja 400</a>
                                            <a href="#">Ninja zx6r</a>
                                            <a href="#">Z800</a>
                                            <a href="#">Ninja H2R</a>
                                        </div>
                                    </div>    
                                </td>
                            </table> 
                        </div>
                        <img className="banner1" src="http://atvillustrated.com/files/Kawi_3lines__logo_web.jpg"></img>
                    </div>
                    <div>
                        <img src={this.props.product.skus[0].image} width="500px"></img>
                        <div className="ninja400">{this.props.product.skus[0].skuname}</div>
                        <div className="ninja400">{this.props.product.skus[0].dimensions.cilindrada}</div>
                        <div className="ninja400de"><del>de {this.props.product.skus[0].listPriceFormated}</del></div>   
                        <div className="ninja400por">por {this.props.product.skus[0].bestPriceFormated}</div>
                    </div>
                    <button className="botão400" onClick={this.carrinho}>Adicionar ao carrinho</button>
                </body>
            </Fragment>
        )
    }
}

export default ProductPage